CREATE TABLE website_ranking
(
  id serial NOT NULL,
  date_visited date,
  website character varying(100),
  visits bigint,
  CONSTRAINT website_ranking_pkey PRIMARY KEY (id)
);

INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-06', 'www.bing.com', 14065457);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-06', 'www.ebay.com.au', 19831166);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-06', 'www.facebook.com', 104346720);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-06', 'mail.live.com', 21536612);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-06', 'www.wikipedia.org', 13246531);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-27', 'www.ebay.com.au', 23154653);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-06', 'au.yahoo.com', 11492756);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-06', 'www.google.com', 26165099);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-13', 'www.youtube.com', 68487810);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-27', 'www.wikipedia.org', 16550230);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-06', 'ninemsn.com.au', 21734381);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-20', 'mail.live.com', 24344783);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-20', 'www.ebay.com.au', 22598506);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-27', 'mail.live.com', 24272437);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-27', 'www.bing.com', 16041776);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-20', 'ninemsn.com.au', 24241574);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-20', 'www.facebook.com', 118984483);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-27', 'ninemsn.com.au', 24521168);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-27', 'www.facebook.com', 123831275);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-20', 'www.bing.com', 16595739);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-13', 'www.facebook.com', 118506019);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-20', 'www.google.com.au', 170020924);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-27', 'www.youtube.com', 69327140);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-13', 'mail.live.com', 24772355);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-13', 'ninemsn.com.au', 24555033);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-20', 'www.google.com', 28996455);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-13', 'www.bing.com', 16618315);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-27', 'www.google.com.au', 171842376);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-06', 'www.youtube.com', 59811438);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-13', 'www.netbank.commbank.com.au', 13316233);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-20', 'www.netbank.commbank.com.au', 13072234);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-13', 'www.ebay.com.au', 22785028);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-20', 'www.wikipedia.org', 16519992);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-27', 'www.bom.gov.au', 14369775);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-27', 'www.google.com', 29422150);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-20', 'www.youtube.com', 69064107);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-06', 'www.google.com.au', 151749278);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-13', 'www.wikipedia.org', 16015926);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-13', 'www.google.com', 29203671);
INSERT INTO website_ranking (date_visited, website, visits) VALUES ('2013-01-13', 'www.google.com.au', 172220397);
