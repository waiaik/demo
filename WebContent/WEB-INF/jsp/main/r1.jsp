<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%> 
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<c:set var="context" value="${pageContext.request.contextPath}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="${context}/js/jquery-ui-1.9.2.custom/js/jquery-1.8.3.js"></script>
<script src="${context}/js/jquery-ui-1.9.2.custom/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="${context}/js/angular.min.js"></script>
<script src="${context}/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="${context}/css/displaytag.css">
<link rel="stylesheet" type="text/css" href="${context}/css/alternative.css">
<link rel="stylesheet" type="text/css" href="${context}/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="${context}/css/bootstrap-theme.min.css">
<link rel="stylesheet" type="text/css" href="${context}/js/jquery-ui-1.9.2.custom/css/smoothness/jquery-ui-1.9.2.custom.min.css">
<link rel="stylesheet" type="text/css" href="${context}/css/style.css">
<link rel="stylesheet" type="text/css" href="${context}/css/form2.css">
<script type="text/javascript">
function split(val) {
    return val.split(/,\s*/);
}
function extractLast(term) {
    return split(term).pop();
}

$(function () {
	var datePicker = angular.module('app', []);
	
	datePicker.directive('jqdatepicker', function () {
	    return {
	        restrict: 'A',
	        require: 'ngModel',
	         link: function (scope, element, attrs, ngModelCtrl) {
	            element.datepicker({
	                dateFormat: 'yyyy-MM-dd',
	                onSelect: function (date) {
	                    scope.date = date;
	                    scope.$apply();
	                }
	            });
	        }
	    };
	});
		
	$( "#dateVisited" ).datepicker({
		dateFormat: "yy-mm-dd",
		showOn: "button",
		buttonImage: "${context}/images/calendar.png",
		buttonImageOnly: true,
		buttonText: "Select date"
	 });
	
	$( "#dateVisited").autocomplete({
        source: function (request, response) {
            $.getJSON("get_date_list", {
                term: extractLast(request.term)
            }, response);
        },
        search: function () {
            var term = extractLast(this.value);
            if (term.length < 1) {
                return false;
            }
        },
        focus: function () {
            return false;
        },
        select: function (event, ui) {
            var terms = split(this.value);
	        terms.pop();
            terms.push(ui.item.value);
            terms.push("");
            this.value = terms.join("");
            return false;
        }
    });
});
</script>
<title>:: demo - websites ranking</title>
</head>
<body>
	<spring:bind path="websiteRankingSearch.*">        
        <c:forEach var="error" items="${status.errorMessages}">
            <div class="error"><li><c:out value="${error}"/></li></div>
        </c:forEach>
    </spring:bind>
	<div id="container">		
		<div id="navigation">
			<jsp:include page="navigation.jsp" />			
		</div>
		<div id="container">
			<div id="content">
				<h4>
					<a href="show">Report</a><span style="font-size:80%;"> >> </span>Top Websites Ranking
				</h4>									
				<span style="font-size:80%" class="required">Denote mandatory fields</span>									
				<form class="feedbackform" method="POST" action="searchR1">														
					<spring:bind path="websiteRankingSearch.dateVisited">
						<div class="fieldwrapper">
							<label class="styled"><span class="required">Date</span></label>
							<div class="thefield">
								<input type="text" id="${status.expression}" name="${status.expression}" value="${status.value}" placeholder="yyyy-MM-dd" maxlength="10" size="8"/>
							</div>															    
						</div>													
					</spring:bind>
					<spring:bind path="websiteRankingSearch.orderBy">
						<div class="fieldwrapper">
							<label class="styled">Order By</label>
							<div class="thefield">	
							<select name="${status.expression}" id="${status.expression}" onchange="">	                            				                       
	                            <option <c:if test="${status.value == 'visits asc'}" >selected</c:if> value="visits asc">Visits Ascending</option>
	                            <option <c:if test="${status.value == 'visits desc'}" >selected</c:if> value="visits desc">Visits Descending</option>		                            
		                    </select>						
							</div>
						</div>
					</spring:bind>					
					<spring:bind path="websiteRankingSearch.limitBy">
						<div class="fieldwrapper">
							<label class="styled">Limit By</label>
							<div class="thefield">	
								<select name="${status.expression}" id="${status.expression}" onchange="">
									<option <c:if test="${status.value == 3}" >selected</c:if> value="3">3</option>	                            				                       
		                            <option <c:if test="${status.value == 5}" >selected</c:if> value="5">5</option>
		                            <option <c:if test="${status.value == 7}" >selected</c:if> value="7">7</option>
		                            <option <c:if test="${status.value == 9}" >selected</c:if> value="9">9</option>
		                            <option <c:if test="${empty status.value}" >selected</c:if> value="">No limit</option>
			                    </select>						
							</div>
						</div>
					</spring:bind>																						
					<div class="buttonsdiv">
						<input type="submit" class="button" value="Submit">
					</div>
				</form>																													
				<div id="displaytag">				
					<display:table name="dataR1" class="simple nocol" export="true" id="currentRowObject" requestURI="searchR1">
					  	<display:setProperty name="basic.empty.showtable" value="true"/>				  					  
					  	<display:setProperty name="export.xml" value="false" />
					  	<display:setProperty name="export.csv" value="false" />
					  	<display:setProperty name="export.excel" value="true" />
					  	<display:setProperty name="export.pdf" value="true" />				  	
					    <display:setProperty name="export.excel.filename" value="top_websites_ranking.xls"/>
					    <display:setProperty name="export.pdf.filename" value="top_websites_ranking.pdf" />				    		    				    				    			    				   				    				  
		   				<display:caption media="html">
					    	<strong>Top Websites Ranking</strong>
					    </display:caption>
					    <display:caption media="excel pdf">Top Websites Ranking</display:caption>
					    <display:column property="dateVisited" title="Date" format="{0,date,yyyy-MM-dd}"/>
					    <display:column property="website" title="Website" style="width:300px;"/>				    
					    <display:column property="visits" title="Visits" format="{0,number,0,000}" style="width:200px; text-align:right;"/>
				 	</display:table>
			 	</div>
		 	</div>	
			<div id="footer">
				<jsp:include page="copyright.jsp" />
			</div>					
		</div>
	</div>
</body>
</html>