<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<c:set var="context" value="${pageContext.request.contextPath}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="${context}/css/displaytag.css">
<link rel="stylesheet" type="text/css" href="${context}/css/alternative.css">
<link rel="stylesheet" type="text/css" href="${context}/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="${context}/css/bootstrap-theme.min.css">
<link rel="stylesheet" type="text/css" href="${context}/js/jquery-ui-1.9.2.custom/css/smoothness/jquery-ui-1.9.2.custom.min.css">
<link rel="stylesheet" type="text/css" href="${context}/css/style.css">
<link rel="stylesheet" type="text/css" href="${context}/css/form2.css">
<title>:: demo - websites ranking</title>
</head>
<body>
	<div id="container">		
		<div id="navigation">
			<jsp:include page="navigation.jsp" />			
		</div>
		<div id="container">
			<div id="content">
				<h4>
					Report
				</h4>
				<p>										
					<ol>
					  <li><a href="r1">Top Websites Ranking</a> <span style="font-size:90%;">(datepicker/autocomplete)</span></li>
					  <li><a href="r2">Websites Ranking - Ratio to Report</a> <span style="font-size:90%;">(sortable column header)</span></li>					  
					</ol>												
				</p>
			</div>			
			<div id="footer">
				<jsp:include page="copyright.jsp" />
			</div>					
		</div>
	</div>
</body>
</html>