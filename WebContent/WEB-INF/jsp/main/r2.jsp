<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%> 
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<c:set var="context" value="${pageContext.request.contextPath}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="${context}/js/jquery-ui-1.9.2.custom/js/jquery-1.8.3.js"></script>
<script src="${context}/js/jquery-ui-1.9.2.custom/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="${context}/js/angular.min.js"></script>
<script src="${context}/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="${context}/css/displaytag.css">
<link rel="stylesheet" type="text/css" href="${context}/css/alternative.css">
<link rel="stylesheet" type="text/css" href="${context}/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="${context}/css/bootstrap-theme.min.css">
<link rel="stylesheet" type="text/css" href="${context}/js/jquery-ui-1.9.2.custom/css/smoothness/jquery-ui-1.9.2.custom.min.css">
<link rel="stylesheet" type="text/css" href="${context}/css/style.css">
<link rel="stylesheet" type="text/css" href="${context}/css/form2.css">
<script type="text/javascript">
$(function () {
	var datePicker = angular.module('app', []);
	
	datePicker.directive('jqdatepicker', function () {
	    return {
	        restrict: 'A',
	        require: 'ngModel',
	         link: function (scope, element, attrs, ngModelCtrl) {
	            element.datepicker({
	                dateFormat: 'yyyy-MM-dd',
	                onSelect: function (date) {
	                    scope.date = date;
	                    scope.$apply();
	                }
	            });
	        }
	    };
	});
	
	$( "#dateVisited" ).datepicker({ dateFormat: "yy-mm-dd" });	        
});
</script>
<title>:: demo - websites ranking</title>
</head>
<body>	
	<div id="container">		
		<div id="navigation">
			<jsp:include page="navigation.jsp" />			
		</div>
		<div id="container">
			<div id="content">
				<h4>
					<a href="show">Report</a><span style="font-size:80%;"> >> </span>Websites Ranking - Ratio to Report
				</h4>																																																			
				<div id="displaytag">				
					<display:table name="dataR2" class="simple nocol" export="true" id="currentRowObject" requestURI="r2">
					  	<display:setProperty name="basic.empty.showtable" value="true"/>				  					  
					  	<display:setProperty name="export.xml" value="false" />
					  	<display:setProperty name="export.csv" value="false" />
					  	<display:setProperty name="export.excel" value="true" />
					  	<display:setProperty name="export.pdf" value="true" />				  	
					    <display:setProperty name="export.excel.filename" value="websites_ranking_ratio.xls"/>
					    <display:setProperty name="export.pdf.filename" value="websites_ranking_ratio.pdf" />				    		    				    				    			    				   				    				  
		   				<display:caption media="html">
					    	<strong>Websites Ranking - Ratio to Report</strong>
					    </display:caption>
					    <display:caption media="excel pdf">Websites Ranking - Ratio to Report</display:caption>					    
					    <display:column property="website" title="Website" sortable="true" headerClass="sortable" style="width:300px;"/>				    
					    <display:column property="visits" title="Visits" format="{0,number,0,000}" sortable="true" headerClass="sortable" style="width:200px; text-align:right;"/>
					    <display:column property="percent" title="Percent (%)" format="{0,number,#.##}%" sortable="true" headerClass="sortable" style="width:200px; text-align:right;"/>
				 	</display:table>
			 	</div>
		 	</div>	
			<div id="footer">
				<jsp:include page="copyright.jsp" />
			</div>					
		</div>
	</div>
</body>
</html>