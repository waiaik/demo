/**
 * 
 */
package com.lwa.demo.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.WebUtils;

import com.lwa.demo.dao.WebsiteRankingMapper;
import com.lwa.demo.model.WebsiteRanking;
import com.lwa.demo.model.WebsiteRankingExample;
import com.lwa.demo.model.WebsiteRankingExample.Criteria;

/**
 * @author waiaik
 *
 */

@Controller
public class MainController{		
	
	@Autowired
	private WebsiteRankingMapper websiteRankingMapper;
	
	@RequestMapping("/show")
	public String showMain(ModelMap model) {      
      return "main";
	}

	@RequestMapping("/r1")
	public String showR1(HttpServletRequest request, ModelMap model) {
		WebsiteRanking websiteRankingSearch = new WebsiteRanking();
		//request.getSession().removeAttribute("websiteRankingSearch");
		websiteRankingSearch.setOrderBy("visits desc");
		websiteRankingSearch.setLimitBy("5");
		model.addAttribute("websiteRankingSearch", websiteRankingSearch);
		return "r1";
	}
	
	@RequestMapping("/r2")
	public String showR2(HttpServletRequest request, ModelMap model) {
		List<WebsiteRanking> list = null;
		WebsiteRankingExample example = new WebsiteRankingExample();
		list = websiteRankingMapper.customSelectRatioToReport(example);
		model.addAttribute("dataR2", list);
		return "r2";
	}
	
	@RequestMapping("/searchR1")
	public String searchR1(HttpServletRequest request, 
			ModelMap model,
			@ModelAttribute("websiteRankingSearch") @Validated WebsiteRanking websiteRankingSearch,
			BindingResult result) {
		boolean search = true;	
		if(result.hasErrors()) {
            return "r1";
        }		
		
		List<WebsiteRanking> list = null;
		//WebsiteRanking websiteRankingSearch = (WebsiteRanking)WebUtils.getSessionAttribute(request, "websiteRankingSearch");
		WebsiteRankingExample example = new WebsiteRankingExample();
		Criteria criteria = example.createCriteria();
		if (websiteRankingSearch==null) {			
			websiteRankingSearch = new WebsiteRanking();			
		}
		
		if (websiteRankingSearch.getDateVisited()==null) {
			search = false;
			result.reject("EmptyDate", "Date is required!");
			return "r1";
		}
		if (search) {
			criteria.andDateVisitedEqualTo(websiteRankingSearch.getDateVisited());
			example.setOrderByClause(websiteRankingSearch.getOrderBy());
			example.setLimitBy(websiteRankingSearch.getLimitBy());
			list = websiteRankingMapper.customSelectByExample1(example);
		}
		WebUtils.setSessionAttribute(request, "websiteRankingSearch", websiteRankingSearch);
		model.addAttribute("dataR1", list);				
		return "r1";
	}
	
	@RequestMapping(value = "/get_date_list", 
			method = RequestMethod.GET,
			produces = "application/json",
			headers="Accept=application/xml, application/json")
	public @ResponseBody List<String> getDateList(@RequestParam("term") String param) {			
		List<String> dateList = websiteRankingMapper.customSelectGroupByDateVisited(param);
		return dateList;			
	}			
	
	@InitBinder    
    public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		// bind string to date
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
        // bind empty strings as null
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }
	
}